/**
 * @file
 * Defines Javascript behaviors for the web3_signer:metamask plugin form.
 */
(function (Drupal) {
  'use strict';

  /*
   * Request accounts from web3 wallet.
   */
  Drupal.AjaxCommands.prototype.walletCommand = async function (ajax, response, status) {
    // Avoid infinite loop, just in case.
    if (ajax.options.data._triggering_element_name == 'walletCommand') {
      return;
    }

    const messages = new Drupal.Message();

    const ethereum = typeof window.ethereum == 'object' ? window.ethereum : false;
    if (!ethereum) {
      messages.clear();
      messages.add(Drupal.t('Ethereum not enabled in this browser'), { type: 'error' });
      return;
    }

    await ethereum.request({
      method: response.method,
      params: response.params,
    })
      .then(function (results) {
        messages.clear();
        messages.add(
          Drupal.t('Method: @method', { '@method': response.method }) + '<br>' +
          Drupal.t('Result: @result', { '@result': Array.isArray(results) ? results.join('<br>') : results })
        );

        // Resend information to Drupal.
        ajax.options.data._triggering_element_name = 'walletCommand';
        ajax.options.data._triggering_element_value = {
          success: true,
          method: response.method,
          data: results
        };
        ajax.$form.ajaxSubmit(ajax.options);
      })
      .catch(function (error) {
        messages.clear();
        messages.add(
          Drupal.t('Code: @code', { '@code': error.code }) + '<br>' +
          Drupal.t('Message: @message', { '@message': error.message }),
          { type: 'error' }
        );
      });
  };

})(Drupal);
