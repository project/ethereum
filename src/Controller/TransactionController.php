<?php

namespace Drupal\ethereum\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\ethereum\Form\EVMTransactionForm;
use Drupal\ethereum\Form\EVMTransactionWatchForm;
use Drupal\taxonomy\TermInterface;

/**
 * Controller routines for ethereum transaction routes.
 */
class TransactionController extends ControllerBase {

  /**
   * Create a transaction.
   *
   * @return array
   *   The form, as a render array.
   */
  public function form() {
    $build = [];
    $build['#title'] = $this->t('New transaction');
    $build['form'] = $this->formBuilder()->getForm(EVMTransactionForm::class);
    return $build;
  }

  /**
   * Create a transaction.
   *
   * @param string $hash
   *   (optional) A given transaction hash.
   * @param \Drupal\taxonomy\TermInterface $network
   *   (optional) A given `ethereum_network` taxonomy term.
   *
   * @return array
   *   The result, as a render array.
   */
  public function watch(string $hash = NULL, TermInterface $network = NULL) {
    $build = [];
    $build['#title'] = $this->t('Transaction');
    $build['form'] = $this->formBuilder()->getForm(EVMTransactionWatchForm::class, $hash, $network);
    return $build;
  }
}
