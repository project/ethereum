<?php

namespace Drupal\ethereum\ContextProvider;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Context\Context;
use Drupal\Core\Plugin\Context\ContextProviderInterface;
use Drupal\Core\Plugin\Context\EntityContextDefinition;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\taxonomy\TermInterface;

/**
 * Sets the an Ethereum network as a context.
 *
 * @group ethereum
 */
class CurrentNetworkContext implements ContextProviderInterface {

  use StringTranslationTrait;

  /**
   * The cache object.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The ethereum_network taxonomy_term storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $termStorage;

  /**
   * The current user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $currentUser;

  /**
   * Constructs a new CurrentNetworkContext.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache.
   * @param \Drupal\Core\Routing\RouteMatchInterface $current_route_match
   *   The route match handler.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct(CacheBackendInterface $cache, RouteMatchInterface $current_route_match, EntityTypeManagerInterface $entity_type_manager, AccountInterface $current_user) {
    $this->cache = $cache;
    $this->routeMatch = $current_route_match;
    $this->termStorage = $entity_type_manager->getStorage('taxonomy_term');
    $this->currentUser = $entity_type_manager->getStorage('user')->load($current_user->id());
  }

  /**
   * Return the plugin context definition.
   *
   * @return \Drupal\Core\Plugin\Context\EntityContextDefinition
   *   The definition.
   */
  public static function getContextDefinition() {
    return EntityContextDefinition::create('taxonomy_term')
      ->setLabel(t("User's preferred Ethereum network"))
      ->setRequired(FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function getRuntimeContexts(array $unqualified_context_ids) {
    $result = [];

    foreach ($unqualified_context_ids as $context_id) {
      switch ($context_id) {
        case 'ethereum_network':
          $term = NULL;
          $vid = 'ethereum_network';
          $context_definition = self::getContextDefinition();

          // Get network from route.
          $term = $this->routeMatch->getParameter('network');

          // Get network from cache by user.
          $cid = 'ethereum:network:' . $this->currentUser->id();
          if (!$term instanceof TermInterface && ($cache = $this->cache->get($cid))) {
            $tid = $cache->data;
            $term = $tid ? $this->termStorage->load($tid) : NULL;
          }

          // Get preferred network from current user.
          if (!$term instanceof TermInterface && $this->currentUser->hasField('ethereum_network')) {
            $terms = $this->currentUser->get('ethereum_network')->referencedEntities();
            $term = reset($terms) ?? NULL;
          }

          // Get default network.
          if (!$term instanceof TermInterface) {
            $tids = $this->termStorage->getQuery()
              ->condition('vid', $vid)
              ->condition('status', 1)
              ->range(0, 1)
              ->accessCheck(TRUE)
              ->execute();
            $terms = !empty($tids) ? $this->termStorage->loadMultiple($tids) : [];
            $term = reset($terms) ?? NULL;
          }

          // Create a default term.
          if (!$term instanceof TermInterface) {
            /** @var \Drupal\taxonomy\TermInterface $term */
            $term = $this->termStorage->create([
              'vid' => $vid,
              'name' => 'Ganache',
            ]);
            if ($term->hasField('chain_id')) {
              $term->set('chain_id', 1337);
            }
          }

          $cacheability = new CacheableMetadata();
          $cacheability->setCacheContexts(['session', 'user']);

          $context = new Context($context_definition, $term);
          $context->addCacheableDependency($cacheability);
          $result[$context_id] = $context;
          break;
      }
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getAvailableContexts() {
    return $this->getRuntimeContexts(['ethereum_network']);
  }
}
