<?php

/**
 * Helper class to prepare and sign EVM-compatible transactions.
 */

namespace Drupal\ethereum\Utility;

use Web3p\EthereumTx\Transaction as Legacy;
use Web3p\EthereumTx\EIP2930Transaction;
use Web3p\EthereumTx\EIP1559Transaction;

/**
 * Prepare transaction to send to ETH networks.
 *
 * @see https://docs.infura.io/infura/networks/ethereum/concepts/transaction-types
 * @see https://github.com/ethereum/go-ethereum/issues/24661#issuecomment-1111938069
 */
class Transaction {

  /**
   * The typed transaction object.
   *
   * Used for signing the transaction data.
   *
   * @var Web3p\EthereumTx\TypeTransaction;
   *
   * @see https://github.com/web3p/ethereum-tx/blob/master/src/Transaction.php
   */
  protected $tx;

  /**
   * The transaction type.
   *
   * @var int
   */
  public $type;

  /**
   * The original values, provided on build.
   *
   * @var array
   */
  public $values;

  /**
   * The transaction data, prepared for sending.
   *
   * @var array
   */
  public $data;

  /**
   * Instanciate a new Transaction.
   *
   * @param array $values
   *   A given list of values.
   * @param int $type
   *   (optional) A given transaction type. Can be 0, 1 or 2 (default: 2).
   */
  public function __construct(array $values, int $type = NULL) {
    $type = $type ?: ($values['type'] ?? 2);
    $this->type = dechex($type);
    $this->values = $values;
    $this->data = $this->prepareData($values);

    // Need to decode chainId because of a bug in Web3php library.
    $params = $this->data;
    if (isset($params['chainId']) && (strpos($params['chainId'], '0x') === 0)) {
      $params['chainId'] = hexdec($params['chainId']);
    }

    switch ($type) {
      case '0x0':
        $this->tx = @new Legacy($params);
        break;
      case '0x1':
        $this->tx = @new EIP2930Transaction($params);
        break;
      default:
        $this->tx = @new EIP1559Transaction($params);
        break;
    }
  }

  /**
   * Help to clean values.
   *
   * @param array $values
   *   A given list of values.
   *
   * @return array
   *  A raw ethereum transaction data.
   */
  public function prepareData(array $values) {
    $minerTip = ($values['maxPriorityFeePerGas'] ?? NULL) ?: 1;
    $minerTip = strpos($minerTip, '0x') === 0 ? hexdec($minerTip) : $minerTip;

    $baseFeePerGas = ($values['baseFeePerGas'] ?? NULL) ?: 0;
    $baseFeePerGas = strpos($baseFeePerGas, '0x') === 0 ? hexdec($baseFeePerGas) : $baseFeePerGas;

    $maxFeePerGas = ($values['maxFeePerGas'] ?? NULL) ?: $baseFeePerGas;
    $maxFeePerGas = strpos($maxFeePerGas, '0x') === 0 ? hexdec($maxFeePerGas) : $maxFeePerGas;
    $maxFeePerGas += $minerTip;

    $data = [
      // Sender.
      'from' => ($values['from'] ?? NULL) ?: NULL,
      // Recipient (required).
      'to' => ($values['to'] ?? NULL) ?: NULL,
      // Maximum amount that be used for this transaction.
      'gas' => $values['gas'] ?? $values['gasLimit'] ?? 21000,
      // Price of the fuel.
      'gasPrice' => $values['gasPrice'] ?? NULL,
      // Maximum fee per gas that the sender is willing to pay.
      'maxFeePerGas' => $maxFeePerGas,
      // Miner tip (default: 1 wei).
      'maxPriorityFeePerGas' => $minerTip,
      // Amount to send, in wei.
      'value' => ($values['value'] ?? NULL) ?: 0,
      // Required. ID of the targeted network as an integer (default: 1337 Ganache e.g. local blockchain).
      'chainId' => ($values['chainId'] ?? NULL) ?: 1337,
      // Account's transaction count.
      'nonce' => ($values['nonce'] ?? NULL) ?: 0,
      // @todo Deploying or calling a contract.
      'data' => ($values['data'] ?? NULL) ?: NULL,
      // @todo Restrict access by address and storage keys.
      'accessList' => ($values['accessList'] ?? NULL) ?: [],
    ];

    // Remove empty values and make sure hex encoding.
    foreach ($data as $key => $value) {
      if (in_array($key, ['from', 'to', 'accessList', 'data'])) {
        continue;
      }

      if (!$value) {
        unset($data[$key]);
      }

      $value = strpos($value ?? '', '0x') === 0 ? $value : (int) $value;
      $value = \is_int($value) ? dechex($value) : $value;
      $data[$key] = (strpos($value, '0x') !== 0) ? '0x' . $value : $value;
    }

    // Filter data by transaction type.
    $fields = ['from', 'to', 'value', 'nonce', 'gas', 'chainId', 'data', 'accessList'];
    switch ($this->type) {
      case '0':
        $fields[] = 'gasPrice';
      case '1':
        $fields[] = 'accessList';
      case '2':
        $fields[] = 'maxFeePerGas';
        $fields[] = 'maxPriorityFeePerGas';
    }

    foreach (array_keys($data) as $key) {
      if (!in_array($key, $fields)) {
        unset($data[$key]);
      }
    }

    return $data;
  }

  /**
   * Update instance's values.
   *
   * @param array $values
   *   A given list of values.
   */
  public function refresh($values = []) {
    $new_data = $values + $this->data;
    $this->data = $this->prepareData($new_data);
  }


  /**
   * Sign the transaction.
   *
   * @param string $private_key
   *  A given hex encoded private key.
   *
   * @return string
   *  The hex encoded signed ethereum transaction.
   */
  public function sign(string $private_key) {
    $this->tx->sign($private_key);
    return '0x' . $this->tx->serialize();
  }
}
