<?php

namespace Drupal\ethereum\PluginForm;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformStateInterface;
use Drupal\web3_provider\PluginForm\Web3ProviderPluginFormBase;

/**
 * Defines a class for common method of plugins' forms.
 */
abstract class EVMProviderPluginFormBase extends Web3ProviderPluginFormBase {

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $plugin = $this->getPlugin();

    if ($form_state instanceof SubformStateInterface) {
      $form_state = $form_state->getCompleteFormState();
    }

    $selected_network = $form_state->getUserInput()['web3_provider']['chainId'] ?? $plugin->getSelectedNetwork();

    $form['chainId'] = [
      '#type' => 'select',
      '#title' => $this->t('Network'),
      '#options' => $plugin->getNetworkOptions(),
      '#default_value' => $selected_network,
      '#required' => TRUE,
    ];

    return $form;
  }
}
