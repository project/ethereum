<?php

namespace Drupal\ethereum\PluginForm;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\ethereum\PluginForm\EVMProviderPluginFormBase;

/**
 * Defines a class for common method of plugins' forms.
 */
class EVMProviderTransactionPluginForm extends EVMProviderPluginFormBase {

  /**
   * {@inheritDoc}
   */
  public function getFormModeId(): string {
    return 'transaction';
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form += parent::buildConfigurationForm($form, $form_state);

    $form['type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Type'),
      '#options' => [
        0 => $this->t('Legacy (before typed transaction)'),
        1 => $this->t('EIP2930 (optional access list)'),
        2 => $this->t('EIP1559 (fee structure)'),
      ],
      '#description' => $this->t('Read more about transaction types @link.', [
        '@link' => Link::fromTextAndUrl(
          $this->t('here'),
          Url::fromUri('https://docs.infura.io/infura/networks/ethereum/concepts/transaction-types')
        )->toString(),
      ]),
      '#default_value' => 2,
      '#required' => TRUE,
    ];

    $form['from'] = [
      // @todo Make this custom `address` element.
      '#type' => 'textfield',
      '#title' => $this->t('From'),
      // @todo Get sender from context.
      // '#default_value' => $context['wallet'],
      '#required' => TRUE,
    ];

    $form['to'] = [
      // @todo Make this custom `address` select element.
      '#type' => 'textfield',
      '#title' => $this->t('To'),
      // @todo Get contact list from context.
      // '#options' => $context['contacts'],
      '#required' => TRUE,
    ];

    $form['value'] = [
      // @todo Make this custom `value` element.
      '#type' => 'number',
      '#title' => $this->t('Value'),
      '#min' => 0,
      // @todo Get sender balance.
      // '#max' => $context['wallet']->getMax(),
      // '#validate' => ['::validateValue'],
      '#description' => $this->t('Integer of the value sent with this transaction.') . '<br>' .
        $this->t('Example: 10000000000 wei = 10 gwei = 0.00000001 ether.') . '<br>' .
        $this->t('Visit the @link for further information.', [
          '@link' => Link::fromTextAndUrl(
            $this->t('online converter'),
            Url::fromUri('https://eth-converter.com/', ['attributes' => [
              'target' => '_blank',
              'title' => $this->t('Open in a new tab'),
            ]]),
          )->toString()
        ]),
      '#default_value' => 0
    ];

    $show_advanced_state = [':input[name="web3_provider[advanced]"]' => ['checked' => TRUE]];

    // Advanced settings.
    $form['advanced'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show advanced settings'),
    ];

    // Base fee hidden value.
    $form['baseFeePerGas'] = [
      '#type' => 'hidden',
      '#title' => $this->t('Base fee per gas'),
    ];

    // Price of the fuel.
    $form['gasPrice'] = [
      // @todo Make this a dynamic element with call to ETH Gas station.
      '#type' => 'number',
      '#title' => $this->t('Gas price'),
      '#description' => $this->t('The price of the fuel on the network.'),
      '#default_value' => 15000000000, // 15 GWEI
      '#states' => [
        'visible' => [
          [
            $show_advanced_state,
            'and',
            ':input[name="web3_provider[type]"]' => ['!value' => '2']
          ]
        ],
      ],
    ];

    // Maximum gas limit.
    $form['gasLimit'] = [
      '#type' => 'number',
      '#title' => $this->t('Gas limit'),
      '#description' => $this->t('The maximum amount of gas units that the transaction may be able to consume.'),
      '#min' => 1,
      '#states' => [
        'visible' => [
          [
            $show_advanced_state,
            'and',
            ':input[name="web3_provider[type]"]' => ['!value' => '2']
          ]
        ]
      ],
    ];

    // Miner tip (default: 1 wei).
    $form['maxPriorityFeePerGas'] = [
      '#type' => 'number',
      '#title' => $this->t('Max priority fee per gas'),
      '#description' => $this->t('Represents the part of the transaction fee that goes to the miner.') . '<br>' .
        $this->t('Read more about @link here.', [
          '@link' => Link::fromTextAndUrl(
            $this->t('maxPriorityFeePerGas vs. maxFeePerGas'),
            Url::fromUri('https://docs.alchemy.com/docs/maxpriorityfeepergas-vs-maxfeepergas', [
              'attributes' => ['target' => '_blank'],
            ])
          )->toString(),
        ]),
      '#min' => 1,
      '#states' => [
        'visible' => [
          [
            $show_advanced_state,
            'and',
            ':input[name="web3_provider[type]"]' => ['value' => '2']
          ]
        ]
      ],
    ];

    // maxFeePerGas = baseFeePerGas + maxPriorityFeePerGas
    $form['maxFeePerGas'] = [
      '#type' => 'number',
      '#title' => $this->t('Max fee per gas'),
      '#description' => $this->t('The absolute maximum you are willing to pay per unit of gas for this transaction  (base fee and max priority fee included).') . '<br>' .
        $this->t('Read more about @link here.', [
          '@link' => Link::fromTextAndUrl(
            $this->t('how EIP-1559 gas fee works'),
            Url::fromUri('https://metamask.io/1559', [
              'attributes' => ['target' => '_blank'],
            ])
          )->toString(),
        ]),
      '#states' => [
        'visible' => [
          [
            $show_advanced_state,
            'and',
            ':input[name="web3_provider[type]"]' => ['value' => '2']
          ]
        ]
      ],
    ];

    // Nonce.
    $form['nonce'] = [
      '#type' => 'number',
      '#title' => $this->t('Nonce'),
      '#description' => $this->t('If empty, nonce value will be calculated automatically before sending the transaction'),
      '#min' => 0,
      '#states' => ['visible' => $show_advanced_state],
    ];

    $form['data'] = [
      // @todo Make this custom `value` element.
      '#type' => 'textarea',
      '#title' => $this->t('Data'),
      // @todo Get sender balance.
      // '#max' => $context['data'],
      '#states' => ['visible' => $show_advanced_state],
    ];

    // @todo Access list?
    $form['accessList'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Access list'),
      '#states' => [
        'visible' => [
          [
            $show_advanced_state,
            'and',
            ':input[name="web3_provider[type]"]' => ['!value' => '0']
          ]
        ]
      ],
    ];

    $form['actions'] = ['#type' => 'actions'];

    $plugin = $this->getPlugin();
    if (method_exists($plugin, 'buildTransactionForm')) {
      if ($form_state instanceof SubformStateInterface) {
        $form_state = $form_state->getCompleteFormState();
      }
      $plugin->buildTransactionForm($form, $form_state);
    }

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $plugin = $this->getPlugin();
    if (method_exists($plugin, 'validateTransactionForm')) {
      $plugin->validateTransactionForm($form, $form_state);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $plugin = $this->getPlugin();
    if (method_exists($plugin, 'submitTransactionForm')) {
      $plugin->submitTransactionForm($form, $form_state);
    }
  }
}
