<?php

namespace Drupal\ethereum\PluginForm;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\ethereum\PluginForm\EVMProviderPluginFormBase;

/**
 * Defines a class for common method of plugins' forms.
 */
class EVMProviderTransactionWatchPluginForm extends EVMProviderPluginFormBase {

  /**
   * {@inheritDoc}
   */
  public function getFormModeId(): string {
    return 'watch';
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, string $hash = NULL) {
    $form += parent::buildConfigurationForm($form, $form_state);

    $form['hash'] = [
      // @todo Make this custom `hash` element.
      '#type' => 'textfield',
      '#title' => $this->t('Hash'),
      '#default_value' => $hash,
      '#required' => TRUE,
    ];

    $form['actions'] = ['#type' => 'actions'];

    $ajax = ['callback' => [get_called_class(), 'ajaxSubmit']];

    $form['actions']['refresh'] = [
      '#op' => 'eth_getTransactionByHash',
      '#type' => 'button',
      '#value' => $this->t('Refresh'),
      '#ajax' => $ajax,
      '#weight' => 1,
    ];

    $form['actions']['explorer'] = [
      '#type' => 'link',
      '#title' => $this->t('View on block explorer'),
      '#url' => Url::fromRoute('<current>'),
      '#attributes' => ['class' => ['button', 'button--secondary']],
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $plugin = $this->getPlugin();
    if (method_exists($plugin, 'validateWatchForm')) {
      $plugin->validateWatchForm($form, $form_state);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $plugin = $this->getPlugin();
    if (method_exists($plugin, 'submitWatchForm')) {
      $plugin->submitWatchForm($form, $form_state);
    }
  }
}
