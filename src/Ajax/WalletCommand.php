<?php

namespace Drupal\ethereum\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * Provides an AJAX command to interact with a web3 wallet.
 *
 * This command is implemented in Drupal.AjaxCommands.prototype.walletCommand.
 */
class WalletCommand implements CommandInterface {
  /**
   * @var string
   */
  protected $action;

  /**
   * @var array
   */
  protected $values;

  /**
   * {@inheritDoc}
   */
  public function __construct(string $action, array $values = []) {
    $this->action = $action;
    $this->values = $values;
  }

  /**
   * {@inheritDoc}
   */
  public function render() {
    return [
      'command' => 'walletCommand',
      'method' => $this->action,
      'params' => [$this->values],
    ];
  }
}
