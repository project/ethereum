<?php

namespace Drupal\ethereum\Plugin\Web3Provider;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AlertCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformStateInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Render\Markup;
use Drupal\ethereum\Plugin\Web3Provider\EVMProviderPluginBase;
use Drupal\ethereum\Ajax\ScrollToCommand;
use Drupal\ethereum\Ajax\WalletCommand;
use Drupal\ethereum\Utility\Transaction;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the web3_provider plugin to sign a transaction with a Web3 wallet.
 *
 * @Web3Provider(
 *   id = "ethereum_wallet",
 *   title = @Translation("Ethereum Wallet"),
 *   description = @Translation("Interact with EVM-compatible web3 wallets (e.g. Metamask).")
 * )
 */
class EthereumWallet extends EVMProviderPluginBase {

  use MessengerTrait;

  /**
   * The library discovery service.
   *
   * @var \Drupal\Core\Asset\LibraryDiscovery
   */
  protected $libraryManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->libraryManager = $container->get('library.discovery');
    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $libraries = array_keys($this->libraryManager->getLibrariesByExtension('ethereum'));
    asort($libraries);

    $form['web3_js_library'] = [
      '#type' => 'item',
      '#title' => $this->t('Libraries'),
      '#description' => $this->t('Nothing to configure.') . '<br>' .
        $this->t("This plugin injects the following JavaScript libraries in order to interact with users' wallet:<br>@list", [
          '@list' => Markup::create('<ul><li><code>' . implode('</code></li><li><code>', $libraries) . '</code></li></ul>'),
        ]),
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function buildTransactionForm(array &$form, FormStateInterface $form_state) {
    // Do not need to show these fields.
    $form['type']['#type'] = 'hidden';
    $form['chainId']['#type'] = 'hidden';
    // $form['from']['#type'] = 'hidden';

    // Handle results from wallet Ajax command.
    $input = $form_state->getUserInput();
    $trigger = $input['_triggering_element_name'] ?? NULL;
    $results = $input['_triggering_element_value'] ?? NULL;
    $success = $results['success'] ?? FALSE;
    $method = $results['method'] ?? NULL;
    if ($success && $method && $trigger == 'walletCommand') {
      switch ($method) {
        case 'eth_requestAccounts':
          // Get connected wallet address.
          $form['from']['#value'] = $results['data'][0] ?? NULL;
          break;
        case 'eth_sendTransaction':
          // Stop now if transaction submitted.
          $hash = $results['data'] ?? NULL;
          $chain_id = $values['chainId'] ?? NULL;

          $form = [
            '#theme' => 'ethereum_transaction_confirmation',
            '#hash' => $hash,
            '#chainId' => $chain_id,
            '#weight' => -1,
          ];

          return $form;
      }
    }

    $ajax = [
      'callback' => [get_called_class(), 'ajaxWalletCommand'],
      'progress' => ['type' => 'fullscreen'],
    ];


    // Hidden button alwaysfirst to handle Ajax submit from JS.
    // @see Drupal.AjaxCommands.prototype.walletCommand().
    $form['actions']['wallet_callback'] = [
      '#op' => 'wallet_callback',
      '#type' => 'button',
      '#value' => $this->t('Callback'),
      '#ajax' =>  [
        'callback' => [get_called_class(), 'ajaxRefreshForm'],
        'progress' => ['type' => 'fullscreen'],
      ],
      '#weight' => -9999,
      '#limit_validation_errors' => [],
      '#attributes' => ['class' => ['visually-hidden']],
    ];

    // Actions.
    $form['actions']['reset'] = [
      '#op' => 'reset',
      '#type' => 'button',
      '#value' => $this->t('Reset'),
      '#ajax' =>  [
        'callback' => [get_called_class(), 'ajaxReset'],
        'progress' => ['type' => 'fullscreen'],
      ],
      '#limit_validation_errors' => [],
      '#states' => [
        'disabled' => [
          ':input[name="web3_provider[from]"]' => ['filled' => FALSE]
        ],
        'invisible' => [
          ':input[name="web3_provider[from]"]' => ['filled' => FALSE]
        ],
      ],
      '#attributes' => ['class' => ['button', 'button--danger']],
    ];

    $form['actions']['connect'] = [
      '#op' => 'eth_requestAccounts',
      '#type' => 'button',
      '#value' => $this->t('Connect'),
      '#ajax' => $ajax,
      '#limit_validation_errors' => [],
      '#states' => [
        'visible' => [
          ':input[name="web3_provider[from]"]' => ['filled' => FALSE]
        ],
        'disabled' => [
          ':input[name="web3_provider[from]"]' => ['filled' => TRUE]
        ],
      ],
    ];

    $form['actions']['send'] = [
      '#op' => 'eth_sendTransaction',
      '#type' => 'button',
      '#value' => $this->t('Send'),
      '#ajax' => $ajax,
      '#states' => [
        'disabled' => [
          [':input[name="web3_provider[from]"]' => ['filled' => FALSE]],
          'or',
          [':input[name="web3_provider[to]"]' => ['filled' => FALSE]],
          'or',
          [':input[name="web3_provider[value]"]' => ['filled' => FALSE]],
        ],
      ],
    ];

    $form['#attached']['library'][] = 'core/drupal.ajax';
    $form['#attached']['library'][] = 'ethereum/wallet.command';

    return $form;
  }

  /**
   * Ajax callback to reset form values.
   */
  public static function ajaxRefreshForm(array $form, FormStateInterface $form_state) {
    $form_selector = '[data-drupal-selector="' . $form['#attributes']['data-drupal-selector'] . '"]';

    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand($form_selector, $form));
    return $response;
  }

  /**
   * Ajax callback to reset form values.
   */
  public static function ajaxReset(array $form, FormStateInterface $form_state) {
    $element = &$form['web3_provider'] ?? [];
    $element['from']['#value'] = $element['from']['#default_value'] ?? NULL;
    $element['to']['#value'] = $element['to']['#default_value'] ?? NULL;
    $element['value']['#value'] = $element['value']['#default_value'] ?? NULL;
    $element['chainId']['#value'] = $element['chainId']['#default_value'] ?? NULL;

    return self::ajaxRefreshForm($form, $form_state);
  }

  /**
   * Ajax callback to send a command to JS script.
   */
  public static function ajaxWalletCommand(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    if ($form_state instanceof SubformStateInterface) {
      $form_state->getCompleteFormState();
    }

    // Send request to wallet.
    $values = $form_state->getValue('web3_provider');
    $action = $form_state->getTriggeringElement()['#op'] ?? NULL;
    $transaction = new Transaction($values);
    $params = $transaction->data;
    $response->addCommand(new WalletCommand($action, $params));
    return $response;
  }
}
