<?php

namespace Drupal\ethereum\Plugin\Web3Provider;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\ethereum\Plugin\Web3Provider\EVMProviderPluginBase;
use Drupal\ethereum\Utility\Transaction;
use Web3p\EthereumUtil\Util;

/**
 * Defines a default web3_provider plugin to communicate with ETH nodes.
 *
 * @Web3Provider(
 *   id = "ethereum_custom",
 *   title = @Translation("Ethereum custom provider"),
 *   description = @Translation("Provides HTTP provider and Signer service from secret keys for EVM-compatible transactions.")
 * )
 */
class EthereumCustom extends EVMProviderPluginBase {

  use MessengerTrait;

  /**
   * Ajax callback.
   */
  public static function ajaxRefresh(array &$form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form += parent::buildConfigurationForm($form, $form_state);

    $config = $this->getConfiguration();

    $url_options = ['attributes' => ['target' => '_blank']];

    $form['web3_http_provider'] = [
      '#type' => 'key_select',
      '#title' => $this->t('HTTP Provider'),
      '#description' => '<br>' .
        $this->t('This is usually an <em>authentication</em> key holding a URL including your API key included.') . '<br>' .
        $this->t('Example of recommended provider: @list', [
          '@list' => Markup::create(implode(', ', [
            Link::fromTextAndUrl('Alchemy', Url::fromUri('https://www.alchemy.com/', $url_options))->toString(),
            Link::fromTextAndUrl('Infura', Url::fromUri('https://www.infura.io/', $url_options))->toString(),
            Link::fromTextAndUrl('Amazon Managed Blockchain', Url::fromUri('https://aws.amazon.com/managed-blockchain/', $url_options))->toString(),
            Link::fromTextAndUrl('cloudfare', Url::fromUri('https://www.cloudflare.com/web3/', $url_options))->toString(),
          ]))
        ]),
      '#default_value' => $config['web3_http_provider'] ?? NULL,
      '#required' => TRUE,
      // @todo Custom key type: web3_http_provider
      '#key_filters' => ['type' => 'authentication'],
    ];

    $form['web3_signer'] = [
      '#type' => 'key_select',
      '#title' => $this->t('Signer'),
      '#description' => '<br>' .
        $this->t('This is usually an <em>authentication</em> key holding your private key.'),
      '#default_value' => $config['web3_signer'] ?? NULL,
      '#required' => FALSE,
      // @todo Custom key type: web3_signer
      '#key_filters' => ['type' => 'authentication'],
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function buildTransactionForm(array &$form, FormStateInterface $form_state) {
    $wrapper_id = Html::getId($form_state->getBuildInfo()['form_id']);
    $form['#id'] = $wrapper_id;

    // Stop now if transaction was submitted.
    if ($hash = $form_state->get('hash')) {
      $chain_id = $form_state->getValue('web3_provider')['chainId'] ?? NULL;

      foreach (Element::children($form) as $key) {
        $form[$key]['#access'] = FALSE;
      }
      unset($form['actions']);

      $form['confirmation'] = [
        '#theme' => 'ethereum_transaction_confirmation',
        '#hash' => $hash,
        '#chainId' => $chain_id,
        '#show_watch_link' => FALSE,
        '#weight' => -1,
      ];

      $form['actions']['watch'] = [
        '#type' => 'link',
        '#title' => $this->t('Watch transaction'),
        '#url' => Url::fromRoute('ethereum.transaction.watch', [
          'hash' => $hash,
          'network' => $chain_id,
        ]),
        '#attributes' => [
          'target' => '_blank',
          'class' => ['button', 'button--primary']
        ],
      ];

      $form['actions']['restart'] = [
        '#op' => 'restart',
        '#type' => 'submit',
        '#value' => $this->t('New transaction'),
        '#ajax' => [
          'callback' => '::ajaxRefresh',
          'wrapper' => $wrapper_id,
        ],
      ];

      return $form;
    }

    try {
      // Select network.
      $requests = [];
      $requests[] = ['net_version' => []];
      $results = $this->requests($requests);
      $chain_id = $results[0]['result'] ?? NULL;
      unset($requests, $results);

      if (in_array($chain_id, $form['chainId']['#options'])) {
        $form['chainId']['#default_value'] = $chain_id;
      }

      // Get address from private key.
      if (!($from = $form_state->getValue('from'))) {
        $key_id = $this->configuration['web3_signer'];
        $key = $this->keyRepository->getKey($key_id);
        $secrets = $key->getKeyValues();
        $private_key = reset($secrets) ?? NULL;

        $utils = new Util;
        $from = $utils->publicKeyToAddress(
          $utils->privateKeyToPublicKey($private_key)
        );
      }

      if ($from) {
        $form['from']['#disabled'] = 'disabled';
        $form['from']['#default_value'] = $from;

        // Get latest block info.
        $requests = [];
        $requests[] = ['eth_blockNumber' => []];
        $results = $this->requests($requests);
        $block_number = $results[0]['result'] ?? [];
        unset($requests, $results);

        // Get fees and nonce.
        $requests = [];
        $requests[] = ['eth_getBlockByNumber' => [$block_number, false]];
        $requests[] = ['eth_getTransactionCount' => [$from, 'latest']];
        $results = $this->requests($requests);
        $block_info = $results[0]['result'] ?? [];
        $tx_count = $results[1]['result'] ?? 0;

        if ($baseFeePerGas = $block_info['baseFeePerGas'] ?? NULL) {
          $form['baseFeePerGas']['#default_value'] = hexdec($baseFeePerGas);
          $form['maxFeePerGas']['#default_value'] = hexdec($baseFeePerGas) + 1;
          $form['maxPriorityFeePerGas']['#default_value'] = 1;
        }

        if ($baseFeePerGas = $block_info['baseFeePerGas'] ?? NULL) {
          $baseFeePerGas = hexdec($baseFeePerGas);
        }

        if ($gasLimit = $block_info['gasLimit'] ?? NULL) {
          $form['gasLimit']['#default_value'] = hexdec($gasLimit);
        }

        if ($nonce = $tx_count ?? NULL) {
          $form['nonce']['#default_value'] = hexdec($nonce);
        }
      }
    } catch (\Exception $e) {
      $this->messenger()->addError($e->getMessage());
    }

    // Actions.
    $form['actions'] = ['#type' => 'actions'];

    $form['actions']['send'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send'),
      '#ajax' => [
        'callback' => '::ajaxRefresh',
        'wrapper' => $wrapper_id,
      ],
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function validateTransactionForm(array &$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement()['#op'] ?? NULL;
    if ($trigger == 'restart') {
      return;
    }

    $transaction = new Transaction($form_state->getValues());
    $form_state->set('transaction', $transaction);

    if (!$transaction instanceof Transaction) {
      $form_state->setErrorByName(NULL, $this->t('Error with transaction data.'));
      return;
    }

    $key_id = $this->configuration['web3_signer'];
    if (!$key_id) {
      $form_state->setErrorByName('web3_signer', $this->t('Missing web3_signer.') . '<br>' . $this->t('Please reconfigure this Web3 provider.'));
      return;
    }

    // Retrieve and validate private key for later use in submit.
    $key = $this->keyRepository->getKey($key_id);
    $secrets = $key->getKeyValues();
    $private_key = reset($secrets) ?? NULL;
    if (!$private_key || empty($private_key)) {
      $form_state->setErrorByName('web3_signer', $this->t('Missing or empty private key'));
      return;
    }

    try {
      // Test transaction by estimating cost.
      $requests = [];
      $requests[] = ['eth_estimateGas' => [$transaction->data, 'latest']];
      $results = $this->requests($requests);
      $estimated_gas = $results[0]['result'] ?? NULL;
      if ($error = $results[0]['error']['message'] ?? NULL) {
        throw new \Exception($error);
      }

      // Sign the transaction with private key now.
      $transaction->refresh(['gas' => $estimated_gas]);
      $form_state->set('signed_tx',  $transaction->sign($private_key));
    } catch (\Exception $e) {
      $form_state->setErrorByName(NULL, $e->getMessage());
      return;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function submitTransactionForm(array &$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement()['#op'] ?? NULL;
    if ($trigger == 'restart') {
      $form_state->set('hash', NULL);
      return;
    }

    $transaction = $form_state->get('transaction');
    $this->logger->notice($this->t('Sending transaction:<br>Values:<pre>@values</pre><br>Data:<pre>@data</pre>', [
      '@values' => Json::encode($transaction->values),
      '@data' => Json::encode($transaction->data),
    ]));

    $signed_tx = $form_state->get('signed_tx');

    try {
      $requests = [];
      $requests[] = ['eth_sendRawTransaction' => [$signed_tx]];
      $results = $this->requests($requests);
      $result = reset($results) ?? [];

      if ($error = $result['error']['message'] ?? NULL) {
        throw new \Exception($error);
      }

      // Save transaction hash.
      $hash = $result['result'] ?? NULL;
      $form_state->set('hash', $hash);
    } catch (\Exception $e) {
      $this->messenger()->addError($e->getMessage());
    }

    // @todo Dispatch event.
  }

  /**
   * {@inheritDoc}
   */
  public function buildWatchForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildWatchForm($form, $form_state);

    $contexts = $this->getContexts();

    // @todo
    $chain_id = '5';

    // @todo
    $hash = isset($contexts['ethereum_transaction']) ? $this->getContextValue('ethereum_transaction') : NULL;
    $hash = '0x8fe8479b8f55f0a52856b2284e87aa03c02f8cb3645cd99447783b31102e935e';

    $form['hash']['#default_value'] = $hash;

    // Get network.
    $requests = [];
    $requests[] = ['eth_getTransactionByHash' => [$hash]];
    $results = $this->requests($requests);
    $info = $results[0]['result'] ?? [];
    unset($requests, $results);

    // Transaction details.
    $form['transaction'] = [
      '#theme' => 'ethereum_transaction',
      '#chainId' => $chain_id,
      '#hash' => $hash,
      '#info' => $info,
    ];

    $wrapper_id = Html::getId($form_state->getBuildInfo()['form_id']);
    $form['#id'] = $wrapper_id;

    // Actions.
    $form['actions'] = ['#type' => 'actions'];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Refresh'),
      '#ajax' => [
        'callback' => '::ajaxRefresh',
        'wrapper' => $wrapper_id,
      ],
    ];

    $form['actions']['explorer'] = [
      '#type' => 'link',
      '#title' => $this->t('View on block explorer'),
      '#url' => Url::fromRoute('<current>'),
      '#attributes' => ['class' => ['button', 'button--secondary']],
    ];

    return $form;
  }
}
