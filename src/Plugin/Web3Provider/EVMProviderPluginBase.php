<?php

namespace Drupal\ethereum\Plugin\Web3Provider;

use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\TermInterface;
use Drupal\web3_provider\Plugin\Web3Provider\Web3ProviderPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for Web3Provider plugins.
 */
abstract class EVMProviderPluginBase extends Web3ProviderPluginBase {

  /**
   * The taxonomy term storage.
   *
   * @var \Drupal\taxonomy\TermStorageInterface
   */
  protected $termStorage;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->termStorage = $container->get('entity_type.manager')->getStorage('taxonomy_term');
    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    // @todo Default type
    // @todo Default gas
    // @todo Default gasPrice
    return $form;
  }

  /**
   * Get the list of value for the network select element.
   *
   * @return array
   *   The list of networks, keyed by their chain ID.
   */
  public function getNetworkOptions() {
    $terms = $this->termStorage->loadByProperties(['vid' => 'ethereum_network']);
    foreach ($terms as $term) {
      /** @var \Drupal\taxonomy\TermInterface $term */
      if ($term->hasField('chain_id') && !($items = $term->get('chain_id'))->isEmpty()) {
        $chain_id = $items->getString();
        $chains[$chain_id] = $term->label();
      }
    }

    // Default network list.
    if (empty($chains)) {
      $chains[1337] = 'Ganache';
      $chains[1] = 'Mainnet';
      $chains[3] = 'Ropsten';
      $chains[4] = 'Rinkeby';
      $chains[5] = 'Goerli';
      $chains[11155111] = 'Sepolia';
    }

    return $chains;
  }

  /**
   * Return the current selected chain ID.
   *
   * @return string
   *   A chain ID.
   */
  public function getSelectedNetwork() {
    $contexts = $this->getContexts();
    $context_network = isset($contexts['ethereum_network']) ? $this->getContextValue('ethereum_network') : NULL;
    if ($context_network instanceof TermInterface && $context_network->hasField('chain_id')) {
      $selected_network = $context_network->get('chain_id')->getString();
    }

    return $selected_network;
  }
}
