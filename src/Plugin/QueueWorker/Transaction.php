<?php

namespace Drupal\ethereum\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;

/**
 * A queue worker for updating transaction information.
 *
 * @QueueWorker(
 *   id = "ethereum_transaction",
 *   title = @Translation("Ethereum transaction"),
 *   cron = {"time" = 10}
 * )
 */
class Transaction extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $hash = ($data['hash'] ?? NULL);
    $chainId = ($data['chainId'] ?? NULL);
    $providers = $this->providerManager->getDefinitions();
  }

}
