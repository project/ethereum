<?php

namespace Drupal\ethereum\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\ethereum\Form\NetworkSwitcherForm;

/**
 * Provides a block to test caching.
 *
 * @Block(
 *   id = "ethereum_network_switcher_block",
 *   admin_label = @Translation("Network switcher"),
 *   category = @Translation("Ethereum"),
 *   context_definitions = {
 *     "ethereum_network" = @ContextDefinition("entity:taxonomy_term", label = @Translation("Ethereum network"))
 *   }
 * )
 */
class NetworkSwitcherBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    /** @var \Drupal\taxonomy\TermInterface $term */
    $term = $this->getContextValue('ethereum_network');

    return \Drupal::formBuilder()->getForm(NetworkSwitcherForm::class, $term);
  }
}
