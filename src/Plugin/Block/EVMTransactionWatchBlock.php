<?php

namespace Drupal\ethereum\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\ethereum\Form\TransactionWatcherForm;

/**
 * Provides a block to test caching.
 *
 * @Block(
 *   id = "ethereum_transaction_watch_form",
 *   admin_label = @Translation("Transaction watcher"),
 *   category = @Translation("Ethereum")
 * )
 */
class EVMTransactionWatchBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return \Drupal::formBuilder()->getForm(TransactionWatcherForm::class);
  }
}
