<?php

namespace Drupal\ethereum\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\ethereum\Form\EVMTransactionForm;

/**
 * Provides a block to display a .
 *
 * @Block(
 *   id = "ethereum_transaction_form",
 *   admin_label = @Translation("Transaction form"),
 *   category = @Translation("Ethereum")
 * )
 */
class EVMTransactionBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return \Drupal::formBuilder()->getForm(EVMTransactionForm::class);
  }

}
