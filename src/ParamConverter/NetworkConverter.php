<?php

namespace Drupal\ethereum\ParamConverter;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\Routing\Route;
use Drupal\Core\ParamConverter\ParamConverterInterface;
use Drupal\taxonomy\TermInterface;

/**
 * Provides upcasting for a node entity in preview.
 */
class NetworkConverter implements ParamConverterInterface {

  /**
   * The taxonomy term storage.
   *
   * @var \Drupal\taxonomy\TermStorageInterface
   */
  protected $termStorage;

  /**
   * The list of field attached to taxonomy_term:network entities.
   *
   * @var array
   */
  protected $fieldDefinitions;

  /**
   * Constructs a new NetworkConverter.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager) {
    $this->termStorage = $entity_type_manager->getStorage('taxonomy_term');
    $this->fieldDefinitions = $entity_field_manager->getFieldDefinitions('taxonomy_term', 'ethereum_network');
  }

  /**
   * {@inheritdoc}
   */
  public function convert($value, $definition, $name, array $defaults) {
    $q = $this->termStorage->getQuery();
    $q->condition('vid', 'ethereum_network');
    $q->accessCheck(FALSE);

    // Try to load by name or by chain ID.
    $field = !ctype_digit($value) ? 'name' : 'chain_id';
    if (in_array($field, array_keys($this->fieldDefinitions))) {
      $q->condition($field, $value);
    }

    $tids = $q->execute();
    $term = !empty($tids) ? $this->termStorage->load(reset($tids)) : NULL;
    return $term instanceof TermInterface ? $term : $value;
  }

  /**
   * {@inheritdoc}
   */
  public function applies($definition, $name, Route $route) {
    if (!empty($definition['type']) && $definition['type'] == 'ethereum_network') {
      return TRUE;
    }
    return FALSE;
  }
}
