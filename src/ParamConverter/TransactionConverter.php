<?php

namespace Drupal\ethereum\ParamConverter;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\Routing\Route;
use Drupal\Core\ParamConverter\ParamConverterInterface;

/**
 * Provides upcasting for a node entity in preview.
 */
class TransactionConverter implements ParamConverterInterface {

  /**
   * The taxonomy term storage.
   *
   * @var \Drupal\storage\StorageStorageInterface
   */
  protected $storage;

  /**
   * The list of field attached to taxonomy_term:network entities.
   *
   * @var array
   */
  protected $fieldDefinitions;

  /**
   * Constructs a new NetworkConverter.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager) {
    $this->storage = $entity_type_manager->getStorage('storage');
    $this->fieldDefinitions = $entity_field_manager->getFieldDefinitions('storage', 'transaction');
  }

  /**
   * {@inheritdoc}
   */
  public function convert($value, $definition, $name, array $defaults) {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function applies($definition, $name, Route $route) {
    if (!empty($definition['type']) && $definition['type'] == 'ethereum') {
      return TRUE;
    }
    return FALSE;
  }
}
