<?php

namespace Drupal\ethereum\Form;

use Drupal\ethereum\Form\EVMProviderFormBase;

/**
 * Defines
 */
class EVMTransactionForm extends EVMProviderFormBase {

  /**
   * {@inheritDoc}
   */
  public function getPluginFormId() {
    return 'transaction';
  }
}
