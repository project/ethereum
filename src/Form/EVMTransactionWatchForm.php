<?php

namespace Drupal\ethereum\Form;

use Drupal\ethereum\Form\EVMProviderFormBase;

/**
 * Defines a class to display and/or refresh information about a transaction.
 */
class EVMTransactionWatchForm extends EVMProviderFormBase {

  /**
   * {@inheritDoc}
   */
  public function getPluginFormId() {
    return 'watch';
  }
}
