<?php

namespace Drupal\ethereum\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\taxonomy\TermInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class to select an Ethereum Network and update the cache context.
 */
class NetworkSwitcherForm extends FormBase {

  /**
   * The current account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * The taxonomy term storage.
   *
   * @var \Drupal\taxonomy\TermStorageInterface
   */
  protected $termStorage;

  /**
   * The cache object.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->account = $container->get('current_user');
    $instance->termStorage = $container->get('entity_type.manager')->getStorage('taxonomy_term');
    $instance->cache = $container->get('cache.data');
    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'network_switcher_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, TermInterface $current_network = NULL) {
    // Load available network, respecting user access.
    $tids = $this->termStorage->getQuery()
      ->condition('vid', 'ethereum_network')
      ->condition('status', 1)
      ->accessCheck(TRUE)
      ->execute();

    $terms = !empty($tids) ? $this->termStorage->loadMultiple($tids) : [];

    $options = [];
    foreach ($terms as $term) {
      $options[$term->id()] = $term->label();
    }

    $form['network'] = [
      '#type' => 'select',
      '#options' => $options,
      '#empty_option' => $this->t('- Select -'),
      '#default_value' => $current_network ? $current_network->id() : NULL,
      '#ajax' => ['callback' => [$this, 'ajaxRefresh']],
      '#required' => TRUE,
    ];

    $form['#attached']['library'][] = 'core/drupal.ajax';

    return $form;
  }

  /**
   * Ajax callback.
   */
  public function ajaxRefresh(array $form, FormStateInterface $form_state) {
    $this->submitForm($form, $form_state);

    // Refresh the page.
    $response = new AjaxResponse();
    $response->addCommand(new RedirectCommand(Url::fromRoute('<current>')->toString()));
    return $response;
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Nothing to validate.
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Set cache context.
    $cid = 'ethereum:network:' . $this->account->id();
    $this->cache->set($cid, $form_state->getValue('network'));
  }
}
