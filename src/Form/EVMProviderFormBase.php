<?php

namespace Drupal\ethereum\Form;

use Drupal\Component\Plugin\Context\ContextInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Link;
use Drupal\Core\Plugin\PluginWithFormsInterface;
use Drupal\Core\Url;
use Drupal\web3_provider\Plugin\Web3Provider\Web3ProviderPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Define a class with a select element to embed web3_provider plugin forms.
 */
abstract class EVMProviderFormBase extends FormBase {

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The plugin form manager.
   *
   * @var \Drupal\Core\Plugin\PluginFormFactoryInterface
   */
  protected $pluginFormFactory;

  /**
   * The Web3 Provider manager
   *
   * @var \Drupal\web3_provider\Web3ProviderManagerInterface
   */
  protected $providerManager;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The context repository service.
   *
   * @var \Drupal\Core\Plugin\Context\ContextRepositoryInterface
   */
  protected $contextRepository;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->logger = $container->get('logger.channel.ethereum');
    $instance->pluginFormFactory = $container->get('plugin_form.factory');
    $instance->providerManager = $container->get('plugin.manager.web3_provider');
    $instance->configFactory = $container->get('config.factory');
    $instance->contextRepository = $container->get('context.repository');
    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'transaction_base_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ...$extra) {
    $wrapper_id = Html::getId($form_state->getBuildInfo()['form_id']);
    $form['#id'] = $wrapper_id;

    // Web3 Provider plugin selection.
    $options = array_column($this->providerManager->getDefinitions(), 'title', 'id');
    asort($options);
    unset($options['default']);

    $provider_settings_url = Url::fromRoute('web3_provider.settings', [], [
      'attributes' => ['target' => '_blank']
    ]);

    $provider_description = '';
    if ($provider_settings_url->access()) {
      $provider_description = Link::fromTextAndUrl(
        $this->t('Configure Web3 Providers settings'),
        $provider_settings_url
      )->toString();
    }

    $plugin_id = $this->getProviderId($form_state);

    $form['provider'] = [
      '#op' => 'provider',
      '#type' => 'radios',
      '#title' => $this->t('Web3 provider'),
      '#options' => $options,
      '#empty_option' => $this->t('- Select -'),
      '#required' => TRUE,
      '#default_value' => $plugin_id,
      '#description' => $provider_description,
      '#ajax' => [
        'callback' => '::ajaxRefresh',
        'wrapper' => $wrapper_id,
      ],
    ];

    $form['#tree'] = TRUE;

    // Display web3_provider transaction plugin form.
    // This method works automagically because it calls `getPluginFormId()` to
    // select the plugin form mode (e.g. ethereum_web3_provider_info_alter()).
    $form['web3_provider'] = [];
    $provider = $this->providerManager->createInstance($plugin_id);
    if ($provider instanceof Web3ProviderPluginInterface) {
      if ($plugin_form = $this->getPluginForm($provider)) {
        $subform_state = SubformState::createForSubform($form['web3_provider'], $form, $form_state);
        $form['web3_provider'] = $plugin_form->buildConfigurationForm($form['web3_provider'], $subform_state);
      }
    }

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Nothing to validate when user switch provider.
    $trigger = $form_state->getTriggeringElement()['#op'] ?? NULL;
    if ($trigger == 'provider') {
      return;
    }

    $plugin_id = $this->getProviderId($form_state);
    $provider = $this->providerManager->createInstance($plugin_id, [
      'no_reset_context' => TRUE,
    ]);
    if (!$provider instanceof Web3ProviderPluginInterface) {
      $form_state->setErrorByName('provider', $this->t('Wrong or missing Web3 provider'));
      return;
    }

    if ($plugin_form = $this->getPluginForm($provider)) {
      $subform_state = SubformState::createForSubform($form['web3_provider'], $form, $form_state);
      $plugin_form->validateConfigurationForm($form['web3_provider'], $subform_state);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild();

    // Nothing to submit when user switch provider.
    $trigger = $form_state->getTriggeringElement()['#op'] ?? NULL;
    if ($trigger == 'provider') {
      return;
    }


    $plugin_id = $this->getProviderId($form_state);
    $provider = $this->providerManager->createInstance($plugin_id, [
      'no_reset_context' => TRUE,
    ]);
    if ($plugin_form = $this->getPluginForm($provider)) {
      $subform_state = SubformState::createForSubform($form['web3_provider'], $form, $form_state);
      $plugin_form->submitConfigurationForm($form['web3_provider'], $subform_state);
    }
  }

  /**
   * Ajax callback.
   */
  public static function ajaxRefresh(array $form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * Get a plugin form.
   *
   * This is where
   */
  public function getPluginForm($plugin, $operation = NULL) {
    $operation = $operation ?? $this->getPluginFormId();
    if ($plugin instanceof PluginWithFormsInterface) {
      if ($plugin->hasFormClass($operation)) {
        return $this->pluginFormFactory->createInstance($plugin, $operation);
      }
    }
  }

  /**
   * Set the default form mode of a plugin embed in this form.
   *
   * @return string
   *   The form mode id.
   *
   * @see ethereum_web3_provider_info_alter();
   */
  public function getPluginFormId() {
    return 'transaction';
  }

  /**
   * Get the current selected provider.
   *
   * @return string
   *   The selected plugin id (default: ethereum_wallet).
   */
  public function getProviderId(FormStateInterface $form_state) {
    $context_id = '@web3_provider.current_provider_context:web3_provider';
    $contexts = $this->contextRepository->getRuntimeContexts([$context_id]);
    $context = $contexts[$context_id] ?? NULL;
    $context_plugin_id = $context instanceof ContextInterface ? $context->getContextValue() : NULL;

    return $form_state->getUserInput()['provider'] ??
      $form_state->getValue('provider') ??
      $context_plugin_id ??
      $this->configFactory->get('web3_provider.settings')->get('default_provider') ??
      'ethereum_wallet';
  }
}
