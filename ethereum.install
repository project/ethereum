<?php

/**
 * @file
 * Installation functions for ethereum module.
 */

use Drupal\taxonomy\TermInterface;

/**
 * Implements hook_install().
 */
function ethereum_install() {
  $chains = [];
  $chains[1337] = 'Ganache';
  $chains[1] = 'Mainnet';
  $chains[3] = 'Ropsten';
  $chains[4] = 'Rinkeby';
  $chains[5] = 'Goerli';
  $chains[11155111] = 'Sepolia';

  $vid = 'ethereum_network';
  $vocabulary_storage = \Drupal::entityTypeManager()->getStorage('taxonomy_vocabulary');
  if (!$vocabulary_storage->load($vid)) {
    $vocabulary = $vocabulary_storage->create([
      'name' => $vid,
      'description' => t('List of EVM powered networks.'),
    ]);

    if(!$vocabulary->save()) {
      \Drupal::logger('ethereum')->error(t('The `ethereum_network` vocabulary could not be created.'));
      return;
    }
  }

  $term_storage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
  foreach ($chains as $chain_id => $name) {
    $tids = $term_storage->getQuery()
      ->condition('vid', $vid)
      ->condition('chain_id', $chain_id)
      ->accessCheck(FALSE)
      ->execute();
    $tid = reset($tids) ?? NULL;
    $term = $tid ? $term_storage->load($tid) : NULL;
    if (!$term instanceof TermInterface) {
      /** @var \Drupal\taxonomy\TermInterface $term */
      $term = $term_storage->create([
        'vid' => $vid,
        'name' => $name,
        'status' => 1,
      ]);
      if ($term->hasField('chain_id')) {
        $term->set('chain_id', $chain_id);
      }
      $term->save();
    }
  }

  // @todo Import transactions from the latest block of default provider?
}
